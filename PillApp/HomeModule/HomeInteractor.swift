//
//  HomeInteractor.swift
//  PillApp
//
//  Created by Ignasi Casulà on 15/01/2020.
//  Copyright © 2020 AiGnIs. All rights reserved.
//

protocol HomeUseCase {
    
}

class HomeInteractor {
}

extension HomeInteractor : HomeUseCase {
    
}
