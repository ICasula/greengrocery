//
//  HomeModuleBuilder.swift
//  PillApp
//
//  Created by Ignasi Casulà on 15/01/2020.
//  Copyright © 2020 AiGnIs. All rights reserved.
//

import UIKit

class HomeModuleBuilder {
    
    func build() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
        
        let presenter = HomePresenter()
        view.presenter = presenter
        
        return view
    }
}
