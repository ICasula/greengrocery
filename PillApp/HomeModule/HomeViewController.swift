//
//  ViewController.swift
//  PillApp
//
//  Created by Ignasi Casulà on 15/01/2020.
//  Copyright © 2020 AiGnIs. All rights reserved.
//

import UIKit

protocol HomeView {
    
}

class HomeViewController: UIViewController {
    @IBOutlet var helloLabel: UILabel!
    
    var presenter: HomePresentation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
}

extension HomeViewController: HomeView {
    
}
